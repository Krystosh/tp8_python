# Codé par Papy Force X, jeune padawan de l'informatique

def dialogue_mot_de_passe():
    login = input("Entrez votre nom : ")
    mot_de_passe_correct = False
    while not mot_de_passe_correct :
        mot_de_passe = input("Entrez votre mot de passe : ")
        # je vérifie la longueur
        if len(mot_de_passe) < 8:
            longueur_ok = False
        else:
            longueur_ok = True
        # je vérifie s'il y a un chiffre
        chiffre_ok = False
        for lettre in mot_de_passe:
            if lettre.isdigit():
                chiffre_ok = True
        # je vérifie qu'il n'y a pas d'espace
        sans_espace = True
        for lettre in mot_de_passe:
            if lettre == " ":
                sans_espace = False
        # Je gère l'affichage
        if not longueur_ok:
            print("Votre mot de passe doit comporter au moins 8 caractères")
        elif not chiffre_ok:
            print("Votre mot de passe doit comporter au moins un chiffre")
        elif not sans_espace:
            print("Votre mot de passe ne doit pas comporter d'espace")	   
        else:
            mot_de_passe_correct = True        
    print("Votre mot de passe est correct")
    return mot_de_passe

#dialogue_mot_de_passe()

def longueur_ok (mdp) :
    """[Vérifie si le mdp fait au minimum 8car]

    Args:
        mdp ([str]): [Le mot de pase]

    Returns:
        [bool]: [True si sup a 8 False sinon]
    """    
    if len(mdp) >= 8 :
        return True
    return False


def chiffre_ok (mdp) :
    """[Vérifie s'il y a au moins 3 chiffre]

    Args:
        mdp ([str]): [Le mot de passe]

    Returns:
        [bool]: [True si nb False sinon]
    """    
    for l in mdp:
        if l.isdigit() :
            return True
    return False

def sans_espace (mdp) :
    for l in mdp :
        if l == ' ' :
            return False
    return True

def trois_chiffre (mdp) :
    cpt_chiffre = 0
    for elem in mdp :
        if elem.isdigit() :
            cpt_chiffre += 1
            if cpt_chiffre > 2:
                return True
    return False

def deux_chiffre(mdp) : 
    for i in range(1,len(mdp)) :
        if mdp[i].isdigit() and mdp[i-1].isdigit() :
                return False
    return True

def apparait_petit_chiffre(mdp) :
    if len(mdp) != 0 :
        cpt = 0
        nb_petit = mdp[0]
        for elem in mdp :
            if elem.isdigit() :
                if elem < nb_petit :
                    nb_petit = elem
                    cpt = 1
                elif elem == nb_petit :
                    cpt += 1
        if cpt > 1 :
            return False
    return True




def dialogue_mot_de_passe_bis():
    login = input("Entrez votre nom : ")
    mot_de_passe_correct = False
    while not mot_de_passe_correct :
        mot_de_passe = input("Entrez votre mot de passe : ")
        if longueur_ok(mot_de_passe) == False:
            print("Votre mot de passe doit comporter au moins 8 caractères")
        elif  chiffre_ok(mot_de_passe) == False:
            print("Votre mot de passe doit comporter au moins 3 chiffres")
        elif  sans_espace(mot_de_passe) == False:
            print("Votre mot de passe ne doit pas comporter d'espace")	   
        elif trois_chiffre(mot_de_passe) == False :
            print("Votre mot de passe doit comporter au moins 3 chiffres")	 
        elif deux_chiffre(mot_de_passe) == False :
            print("Votre mot de passe ne doit pas avoir deux chiffres qui se suivent")	
        elif apparait_petit_chiffre(mot_de_passe) == False :
            print("Votre mot de passe ne doit pas avoir 2 chiffres petits ")	
        else:
            mot_de_passe_correct = True        
    print("Votre mot de passe est correct")
    return mot_de_passe
dialogue_mot_de_passe_bis()