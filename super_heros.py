# Exercice 2 Des super-héros

avengers = {
'Spiderman': (5, 5, 'araignée a quatre pattes'),
'Hulk': (7, 4, "Grand homme vert"),
'Agent 13': (2, 3, 'agent 13'),
'M Becker': (2, 6, 'expert en graphe'),
}

def intelligence_moy(dic) :
    """[summary]

    Args:
        dic ([dict]): [Un dictionnaire dont les clefs sont les noms de personnage,
        et les valeurs des tuples contenant la force (int), l’intelligence (int)
        et la description (str) de ce personnage]

    Returns:
        [float]: [Retourne l'intellgience moyenne de tous les avengers]
    """    
    intel_moy = 0
    for info in dic.values() :
        intel_moy+= info[1]
    return intel_moy/len(dic)
#print(intelligence_moy(avengers))

def kikelplusfort(dic) :
    res = ''
    force = 0
    for (nom,info) in dic.items() :
        if info[0] > force :
            res = nom
            force = info[0]
    return res
print(kikelplusfort(avengers))