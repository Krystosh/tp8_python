# TP8 B - Manipuler des listes, ensembles et dictionnaires
troupeau_de_jean = {'vache':12, 'cochon':17, 'veau':3}
troupeau_de_perrette = {'veau':14, 'vache':7, 'poule':42}
troupeau_vide = dict()
mon_troupeau = {'vache' : 9, 'brebis' : 12, 'poule' : 36, 'moutons' : 15}

def total_animaux(troupeau):
    """ Calcule le nombre total d'animaux dans un troupeau

    Args:
        troupeau (dict): un dictionnaire modélisant un troupeau {nom_animaux: nombre}

    Returns:
        int: le nombre total d'animaux dans le troupeau
    """
    nb_tot = 0
    for nb_animaux in troupeau.values() :
        nb_tot += nb_animaux
    return nb_tot
#print(total_animaux(mon_troupeau))


def tous_les_animaux(troupeau):
    """ Détermine l'ensemble des animaux dans un troupeau

    Args:
        troupeau (dict): un dictionnaire modélisant un troupeau {nom_animaux: nombre}

    Returns:
        set: l'ensemble des animaux du troupeau
    """
    res = set()
    for nom_animaux in troupeau.keys() :
        res.add(nom_animaux)
    return res
#print(tous_les_animaux(mon_troupeau))


def specialise(troupeau):
    """ Vérifie si le troupeau contient 30 individus ou plus d'un même type d'animal 

    Args:
        troupeau (dict): un dictionnaire modélisant un troupeau {nom_animaux: nombre}

    Returns:
        bool: True si le troupeau contient 30 (ou plus) individus d'un même type d'animal,
        False sinon 
    """
    for nb_animaux in troupeau.values():
        if nb_animaux>29 :
            return True
    return False
#print(specialise(mon_troupeau))

def le_plus_represente(troupeau):
    """ Recherche le nom de l'animal qui a le plus d'individus dans le troupeau
    
    Args:
        troupeau (dict): un dictionnaire modélisant un troupeau {nom_animaux: nombre}

    Returns:
        str: le nom de l'animal qui a le plus d'individus  dans le troupeau
        None si le troupeau est vide) 
    
    """
    res = ''
    nbr= 0
    for (animaux,nb_animaux) in troupeau.items() :
        if nb_animaux > nbr :
            res=animaux
            nbr = nb_animaux
    if len(troupeau) == 0 :
        return None
    return res
#print(le_plus_represente(mon_troupeau))


def quantite_suffisante(troupeau):
    """ Vérifie si le troupeau contient au moins 5 individus de chaque type d'animal

    Args:
        troupeau (dict): un dictionnaire modélisant un troupeau {nom_animaux: nombre}

    Returns:
        bool: True si le troupeau contient au moins 5 individus de chaque type d'animal
        False sinon    
    """
    for nb_animaux in troupeau.values() :
        if nb_animaux < 4 :
            return False
    return True
#print(quantite_suffisante(mon_troupeau))


def reunion_troupeaux(troupeau1, troupeau2):
    """ Simule la réunion de deux troupeaux

    Args:
        troupeau1 (dict): un dictionnaire modélisant un premier troupeau {nom_animaux: nombre}
        troupeau2 (dict): un dictionnaire modélisant un deuxième troupeau        

    Returns:
        dict: le dictionnaire modélisant la réunion des deux troupeaux    
    """
    if len(troupeau2) != 0 :
        res = dict()
        for (anim,nb_anim) in troupeau1.items() :
            res[anim] = nb_anim
        for (animal,nb_animal) in troupeau2.items() :
            if animal not in res.keys() :
                res[animal] = nb_animal
            else :
                res[animal] += nb_animal
    else :
        res = troupeau1
    return res
print(reunion_troupeaux({},troupeau_de_jean))